build: temaA

temaA: temaA.l
	flex temaA.l
	gcc lex.yy.c -lfl -o temaA

test1: temaA
	./temaA test1.in

test2: temaA
	./temaA test2.in

test3: temaA
	./temaA test3.in

.PHONY: clean

clean:
	rm temaA lex.yy.c