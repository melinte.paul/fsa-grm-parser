//Melinte Paul-Eduard
//336CC

//Tema FLEX varianta A

Tema a fost realizata pe distributia de Linux Ubuntu 20

Din cerintele temei, nu s-a indeplinit inlocuirea variabilelor in reguli

//Initial
In starea Initial, se verifica headerul gramaticii sau al automatului, 
se afiseaza tipul si numele, apoi se trece in starea corespunzatoare

//Gramatici

Primele reguli din starile gramaticii au rolul de a salva alfabetul
Dupa aceea se verifica tipul de gramatica, prin setarea unor flag-uri 
La final se afiseaza gramatica cea mai putin restrictiva care are flag-ul 
setat.

Detectarea gramaticiilor regulate se face prin gasirea regulilor de tipul:
	Neterminal -> sir de terminali urmat de un posibil neterminal, 
valoare e, caracterul vid, este considerat un terminal pentru simplificare regulilor.
Gramaticiile independente de context incep tot cu un neterminal, dar pot sa treaca in orice sir.

Gramaticile dependete de context si fara restrictii au aceasi forma, dar ca in cele dependente de context
lungimea sirurilor creste sau ramane aceasi. De aceea la gasirea unei astfel de reguli, setam flagul GDC
si apoi cautam o regula ce scade lungimea, caz in care setam flagul GFR

La finalul gramaticii se afiseaza tipul si alfabetul

//Automate

Primele reguli afiseaza alfabetul, starile, starile initiale, finale, etc.

Cand trecem la regulile automatului, se salveaza starea initiala, pentru a fi afisata de fiecare data.
Urmatoarele reguli parseaza caracterul curent din sir, liteza de pe stiva, sirul ce va fi pus pe stiva si starea urmatoare.
Regula este afisata pe parcurs, in timpul parsarii partilor ei.

//Comentarii

Comentariile pe o singura linie sunt parsate simplu, cu regula ^"%".* , adica un sir ce incepe cu % la inceputul linie 
si toate caractele pana la newline

Comentariile multilinie au o regula mai complexa: "/*"([^*]|\*+[^*/])*\*+"/"
	"/*" : inceputul comentariului
	([^*]|\*+[^*/])* : fie text fara *, fie cel putin o * ce nu este urmata de /, repetat de 0 sau mai multe ori
	\*+"/" : finalul comentariului, precedat de oricate * in plus
